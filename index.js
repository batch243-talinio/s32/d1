
let http = require("http");
const port = 4000;

// HTTP Routing Methods: GET - POST - PUT - DELETE

http.createServer(function(request, response)
	{
		if(request.url == "/items" && request.method == "GET"){
			response.writeHead(200, {'Content-Type': 'text/plain'});
			response.end('Data retrieved from Database!');
		
		}else if(request.url == "/items" && request.method == "POST"){
			response.writeHead(200, {'Content-Type':'text-plain'});
			response.end('Data to be sent to the Database')
		}

		
	}
).listen(port);

console.log(`Server now accessible at localhosts: ${port}`)






// HTTP method of the incoming request can be accessed via the method property of the request parameter.
// the method "GET" means that we will be retrieving or reading an information.