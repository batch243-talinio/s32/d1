let http = require("http");
const port = 4000

// MOCK Database
	let directory = [
		{
			"name": "Brandon",
			"email": "brandon@isekai.com",

		},
		{
			"name": "Jobert",
			"email": "jober@isekai.com",
		},

	]



	const server = http.createServer((request, response) =>
			{
				// ROUTE for returning all items upon recieving a GET request.

				// when sending data to web server, the data has to be in string.
				// we used 'stringify' becz our input should always be in a 'STRING' type since our 'directory' is in object format

				if(request.url == "/users" && request.method == "GET"){
					response.writeHead(200, {'Content-Type':'application/json'});
					response.write(JSON.stringify(directory));
					response.end();
				}

				// "ADDING" user to the database
				if(request.url == "/users" && request.method == "POST"){
					// "DECLARE and INITIALIZE" a 'requestBody' variable to an empty string.
					// this will act as a placeholder for the data to be created later on.

					let requestBody = "";

					// a stream is a sequence of data
					// DATA is received from the client and is procesed in the "data stream".
					//  the information provided from the request object enters a sequence called "data", the code will be triggered.
					// data step - this reads the "data" stream and process it as the request body 

					request.on('data', function(data){

						// assigns the data retrieved from the data stream to the requestBody.
						requestBody += data;
					});

					// response end step - only runs after the request has completely been sent
					request.on('end', function(){

						console.log(typeof requestBody);

						requestBody = JSON.parse(requestBody);

						// create a new object representing the new mock database record.
						let newUser = {
							"name": requestBody.name,
							"email": requestBody.email
						}

						// and the new User into the mock database
						directory.push(newUser);
						console.log(directory);

						response.writeHead(200, {'Content-Type':'application/json'});
						response.write(JSON.stringify(newUser));
						response.end();
					});
				}

			}).listen(port)

console.log(`Server running at localhosts ${port}`)